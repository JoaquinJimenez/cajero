# Resumen

Proyecto realizado para la asignatura PSP del ciclo DAM. Uso de hilos y shocket.

## Descripción del proyecto

Simulación de un programa de una entidad bancaria, existen dos tipos de usuarios, los clientes, que pueden retirar o ingresar dinero, y los gestores, que gestionan los clientes y cuentas.

Los usuarios deberan acceder al mismo programa a la vez desde diferentes dispositivos.
